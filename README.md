# Building projects
## Backend
Navigate to /backend folder and execute
```
mvn clean package
```
It will generate `delfin-1.0.0.jar` file in the /backend/target directory.
Navigate to the target directory and execute
```
java -jar delfin-1.0.0.jar
```
It will launch the app on default port `8086`.
To see available endpoints and try them, navigate to http://localhost:8086/swagger-ui.html

*Note:* All this configuration is valid if default settings are used for the app.

## Frontend
Navigate to /react-frontend folder and execute
```
npm install
npm start
```
Dev app will be available at http://localhost:3000/

*Note:* All this configuration is valid if default settings are used for the app.