import axios from './axios';

export function getArticle(articleId) {
    return axios.get('article/' + articleId);
}