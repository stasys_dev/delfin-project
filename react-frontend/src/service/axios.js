import axios from 'axios';

const BACKEND_DEFAULT_URL = 'http://localhost:8086/';
const BACKEND_BASE_PATH = process.env.REACT_APP_BACKEND_URL || BACKEND_DEFAULT_URL;

const instance = axios.create({
    baseURL: BACKEND_BASE_PATH,
    timeout: 5000
});

export default instance;