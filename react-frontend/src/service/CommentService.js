import axios from './axios';

export function getCommentPages(articleId) {
    return axios.get('/article/' + articleId + '/comments');
}

export function getComments(articleId, page) {
    return axios.get('/article/' + articleId + '/comments/' + page);
}