import React, {Component} from 'react';
import './App.css';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import UrlBar from './components/UrlBar/UrlBar'
import CommentSection from './components/CommentSection/CommentSection';
import ArticleSection from './components/ArticleSection/ArticleSection';
import CommentSectionHeader from './components/CommentSection/CommentSectionHeader/CommentSectionHeader';
import Section from './components/Section/Section';

import {getArticle} from './service/ArticleService';
import {getCommentPages, getComments} from './service/CommentService';

class App extends Component {
    state = {
        article: {},
        articleIdSelected: null,
        commentPages: {},
        showHeader: true,
        articleContentExpanded: false,
        commentContentExpanded: false,
        commentPageSelected: 0,
        commentPageSelectedData: []
    }

    onGoButtonClicked = (value) => {
        let articleId = this.extractId(value);
        if (articleId) {
            getArticle(articleId)
                .then((response) => {
                    this.setState({
                        article: response.data,
                        articleIdSelected: articleId,
                        articleContentExpanded: true
                    });
                }).catch((err) => {
                    this.setState({article: {}, articleIdSelected: null})
                });

            getCommentPages(articleId)
                .then((response) => {
                    this.setState({
                        commentPages: response.data.commentPages,
                        commentContentExpanded: true
                    });
                }).catch((err) => {
                    this.setState({commentPages: {}});
                }).then(() => {
                    this.loadCommentPage(articleId, 0);
                });
        } else {
            // show error message
            this.setState({article: {}});
        }
    }

    loadCommentPage = (articleId, page) => {
        getComments(articleId, page)
            .then((response => {
                this.setState({
                    commentPageSelectedData: response.data,
                    commentPageSelected: page
                });
            }));
    }

    extractId = (urlStr) => {
        var a = document.createElement('a');
        a.href = urlStr;
        if (!a.host) {
            return null;
        }
        let params = new URLSearchParams(a.search.slice(1));
        return params.get('id');
    }

    handleCommentPageChanged = (index) => {
        this.setState({commentPageSelected: index});
        this.loadCommentPage(this.state.articleIdSelected, index);
    }

    render() {
        return (
            <div className="App">
                <div className="App__header">
                    <UrlBar onGoButtonClicked={this.onGoButtonClicked}/>
                </div>
                <Grid fluid>
                    <Row>
                        <Col>
                            <Section
                                headerTitle='Straipsnis'
                                expanded={this.state.articleContentExpanded}
                                onToggle={() => this.setState({articleContentExpanded: !this.state.articleContentExpanded})}>
                                <div className='App__content-area'>
                                    <ArticleSection article={this.state.article}/>
                                </div>
                            </Section>
                            <Section
                                headerTitle='Komentarai'
                                expanded={this.state.commentContentExpanded}
                                onToggle={() => this.setState({
                                    commentContentExpanded:
                                        !this.state.commentContentExpanded
                                })}>

                                <CommentSectionHeader
                                    commentPages={this.state.commentPages}
                                    commentPageSelected={this.state.commentPageSelected}
                                    commentPageSelectedData={this.state.commentPageSelectedData}
                                    onCommentPageChanged={this.handleCommentPageChanged}
                                />
                                <CommentSection
                                    data={this.state.commentPageSelectedData}/>
                            </Section>
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}

export default App;
