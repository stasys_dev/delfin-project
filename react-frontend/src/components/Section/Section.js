import React from 'react';
import Glyphicon from 'react-bootstrap/lib/Glyphicon';
import Panel from 'react-bootstrap/lib/Panel';
import './Section.css';

export default (props) => {
    return (
        <div>
            <div className="Section__header">
                <h3 className="Section__title">{props.headerTitle}</h3>
                <div className='Section__toggle'>
                    <div
                        className="Section__toggle-button"
                        onClick={props.onToggle}>
                        <Glyphicon glyph={(props.expanded ? "chevron-up" : "chevron-down")}/>
                    </div>
                </div>
            </div>
            <Panel
                id="panel-article"
                onToggle={() => {}} // for some reason it is required, although I absolutely don't need this
                expanded={props.expanded}>
                <Panel.Collapse>
                    <Panel.Body>
                        {props.children}
                    </Panel.Body>
                </Panel.Collapse>
            </Panel>
        </div>
    );
}