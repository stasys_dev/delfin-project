import React from 'react';
import ListGroup from 'react-bootstrap/lib/ListGroup';
import CommentItem from './CommentItem/CommentItem';

export default (props) => {
    let commentsData = props.data.reduce((acc, currVal) => {
        acc.push(currVal);
        if (currVal.replies) {
            currVal.replies.forEach((r) => {
                r.reply = true;
                acc.push(r);
            });
        }
        return acc;
    }, []);

    let comments = commentsData.map((c, index) => {
        return <CommentItem key={'ci-' + index} comment={c}/>
    })
    return (
        <ListGroup>
            {comments}
        </ListGroup>
    );
}