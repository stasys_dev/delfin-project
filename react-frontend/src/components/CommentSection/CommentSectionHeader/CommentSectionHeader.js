import React from 'react';
import ControlLabel from "react-bootstrap/lib/ControlLabel";
import Form from "react-bootstrap/lib/Form";
import FormGroup from "react-bootstrap/lib/FormGroup";
import Col from "react-bootstrap/lib/Col";
import DropdownButton from "react-bootstrap/lib/DropdownButton";
import Radio from "react-bootstrap/lib/Radio";
import MenuItem from "react-bootstrap/lib/MenuItem";

export default (props) => {
    let commentPages;
    if (props.commentPages && Object.keys(props.commentPages).length > 0) {
        commentPages = props.commentPages.map((page, index) => {
            return <MenuItem
                key={'cm-page-' + index}
                eventKey={index}
                onSelect={props.onCommentPageChanged}
                active = {props.commentPageSelected === index ? true : false}>{index}</MenuItem>
        });
    }

    return (
        <Form horizontal>
            <FormGroup controlId="formControlsSelect">
                <Col componentClass={ControlLabel} sm={2}>
                    Komentarų puslapis
                </Col>
                <Col sm={1}>
                    <DropdownButton
                        id="dropdown-comment-pages"
                        disabled={(Object.keys(props.commentPageSelectedData).length) !== 0 ? false : true}
                        title={props.commentPageSelected + ''}>
                        {commentPages}
                    </DropdownButton>
                </Col>
                <Col componentClass={ControlLabel} sm={2}>
                    Komentarų tipas
                </Col>
                <Col sm={7}>
                    <Radio disabled name="radioGroup" inline>
                        Registruoti
                    </Radio>{' '}
                    <Radio
                        name="radioGroup"
                        readOnly
                        checked
                        inline>
                        Anoniminiai
                    </Radio>
                </Col>
            </FormGroup>
        </Form>
    );
}