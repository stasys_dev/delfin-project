import React from 'react';
import Label from 'react-bootstrap/lib/Label';
import ListGroupItem from 'react-bootstrap/lib/ListGroupItem';
import Glyphicon from 'react-bootstrap/lib/Glyphicon';
import './CommentItem.css';

export default (props) => {
    let comment = props.comment;
    let styles = ['Comment'];
    if (comment.reply === true) {
        styles.push('Comment--reply');
    }
    return (
        <div className={styles.join(' ')}>
            <ListGroupItem>
                <h4>{comment.author} <Label>{comment.date}</Label></h4>
                <p>{comment.commentText}</p>
                <div>
                    <Glyphicon glyph='thumbs-up'/>
                    {'   ' + comment.likes + '   '}
                    <Glyphicon glyph='thumbs-down'/>
                    {'   ' + comment.dislikes + '   '}
                </div>
            </ListGroupItem>
        </div>
    );
}