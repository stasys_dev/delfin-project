import React from 'react';

export default (props) => {
    let articleContent;

    if (Object.getOwnPropertyNames(props.article).length !== 0) {
        let article = props.article;
        let paragraphs = article.paragraphs.map((p, index) => {
            let paragraphContent;
            if (p.heading === true) {
                paragraphContent = <h3 key={'p' + index}>{p.paragraphText}</h3>;
            } else {
                paragraphContent = <p key={'p' + index}>{p.paragraphText}</p>;
            }
            return paragraphContent;
        });
        articleContent = (
            <div>
                <h1>{article.title}</h1>
                {paragraphs}
            </div>
        );
    }

    return <div>{articleContent}</div>;
}