import React, {Component} from 'react';
import FormGroup from 'react-bootstrap/lib/FormGroup';
import FormControl from 'react-bootstrap/lib/FormControl';
import Button from 'react-bootstrap/lib/Button';
import Form from 'react-bootstrap/lib/Form';
import Col from 'react-bootstrap/lib/Col';
import './UrlBar.css';

class UrlBar extends Component {
    constructor(props) {
        super(props);
        this.inputRef = React.createRef();
    }

    render() {
        return (
            <div>
                <Form horizontal>
                    <FormGroup style={{marginBottom: "0"}} controlId="formHorizontalEmail">
                        <Col sm={10}>
                            <FormControl
                                inputRef={(ref) => this.inputRef = ref}
                                type="input"
                                placeholder="Nuoroda į straipsnį"/>
                        </Col>
                        <Col sm={2}>
                            <div className="UrlBar__button">
                                <Button
                                    onClick={() => this.props.onGoButtonClicked(this.inputRef.value)}
                                    block>Skaityti!</Button>
                            </div>
                        </Col>
                    </FormGroup>
                </Form>
            </div>
        );
    }
}

export default UrlBar;