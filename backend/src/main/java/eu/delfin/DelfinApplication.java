package eu.delfin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DelfinApplication {

    public static void main(String[] args) {
        SpringApplication.run(DelfinApplication.class, args);
    }

}
