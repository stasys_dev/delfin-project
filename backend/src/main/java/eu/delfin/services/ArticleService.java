package eu.delfin.services;

import eu.delfin.api.model.ArticleDto;

import java.util.Optional;

public interface ArticleService {
    Optional<ArticleDto> getArticleFromInternet(String articleId);
}
