package eu.delfin.services;

import eu.delfin.api.model.CommentDto;
import eu.delfin.api.model.CommentPageDto;
import eu.delfin.mappers.CommentMapper;
import eu.delfin.parsers.CommentPageParser;
import eu.delfin.readers.CommentReader;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CommentServiceImpl implements CommentService {
    private CommentPageParser commentPageParser;
    private CommentReader commentReader;
    private CommentMapper commentMapper;

    public CommentServiceImpl(CommentPageParser commentPageParser,
                              CommentReader commentReader,
                              CommentMapper commentMapper) {
        this.commentPageParser = commentPageParser;
        this.commentReader = commentReader;
        this.commentMapper = commentMapper;
    }

    @Override
    public List<CommentPageDto> getListOfAvailableCommentPages(String articleId) {
        String pageHtmlContent = commentReader.getCommentPageContentOrderByNewest(articleId, 0);
        int count = commentPageParser.getNumberOfCommentPages(pageHtmlContent);
        if (count == 0) {
            return Collections.singletonList(new CommentPageDto(0));
        }

        List<CommentPageDto> commentPageDtos = new ArrayList<>(count + 1);
        for (int i = 0; i < count  ; i++) {
            commentPageDtos.add(new CommentPageDto(i));
        }
        return commentPageDtos;
    }

    @Override
    public List<CommentDto> getCommentsOnPage(String articleId, int pageIndex) {
        String htmlPageContent = commentReader.getCommentPageContentOrderByNewest(articleId, pageIndex);
        return commentPageParser.getCommentsOnPage(htmlPageContent)
                .stream()
                .map(comment -> commentMapper.mapToDto(comment))
                .collect(Collectors.toList());
    }
}
