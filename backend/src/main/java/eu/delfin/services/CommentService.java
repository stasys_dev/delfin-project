package eu.delfin.services;

import eu.delfin.api.model.CommentDto;
import eu.delfin.api.model.CommentPageDto;

import java.util.List;

public interface CommentService {
    List<CommentPageDto> getListOfAvailableCommentPages(String articleId);
    List<CommentDto> getCommentsOnPage(String articleId, int pageIndex);
}
