package eu.delfin.services;

import eu.delfin.api.model.ArticleDto;
import eu.delfin.mappers.ArticleMapper;
import eu.delfin.parsers.ArticlePageParser;
import eu.delfin.parsers.data.Article;
import eu.delfin.readers.ArticleReader;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ArticleServiceImpl implements ArticleService {
    private ArticleReader articleReader;
    private ArticlePageParser articlePageParser;
    private ArticleMapper articleMapper;

    public ArticleServiceImpl(ArticleReader articleReader,
                              ArticlePageParser articlePageParser,
                              ArticleMapper articleMapper) {
        this.articleReader = articleReader;
        this.articlePageParser = articlePageParser;
        this.articleMapper = articleMapper;
    }

    public Optional<ArticleDto> getArticleFromInternet(String articleId) {
        Optional<String> articleHtmlContent = articleReader.getArticlePageContent(articleId);
        if (articleHtmlContent.isPresent()) {
            Article article = articlePageParser.parseArticle(articleHtmlContent.get());
            article.setArticleExternalId(Long.parseLong(articleId));
            return Optional.of(articleMapper.mapToDto(article));
        }
        return Optional.empty();
    }
}
