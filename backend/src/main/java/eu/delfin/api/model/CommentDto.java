package eu.delfin.api.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@ApiModel(description = "Representation of a comment")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommentDto {
    @ApiModelProperty(value = "Comment author's name.")
    private String author;
    @ApiModelProperty(value = "Unique article id found in article url", example = "78424045")
    private String delfiId;
    // TODO: externalize date serialization config
    @ApiModelProperty(value = "Date when a comment was written. yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime date;
    private String commentText;
    private int likes;
    private int dislikes;
    private List<CommentDto> replies;
}
