package eu.delfin.api.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.hateoas.ResourceSupport;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@ApiModel(description = "Representation of an article")
@Data
public class ArticleDto extends ResourceSupport {
    @ApiModelProperty
    private String title;
    @ApiModelProperty
    private String description;
    @ApiModelProperty(value = "Unique article id found in article url", example = "78424045")
    private Long articleExternalId;
    private String authorName;
    @ApiModelProperty(value = "Additional information about the article author")
    private String authorTitle;
    // TODO: externalize date serialization config
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime articlePublishedDate;
    @ApiModelProperty(value = "Source of an article, like local or foreign news portals")
    private String source;
    private List<ParagraphDto> paragraphs = new ArrayList<>();
}
