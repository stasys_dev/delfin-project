package eu.delfin.api.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.ResourceSupport;

@ApiModel(description = "Representation of a comment page")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommentPageDto extends ResourceSupport {
    @ApiModelProperty(value = "Index of comment page. Starts at index 0.")
    private int pageIndex;
}
