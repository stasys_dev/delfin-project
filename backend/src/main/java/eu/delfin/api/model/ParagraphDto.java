package eu.delfin.api.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel(description = "Representation of article paragraph")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ParagraphDto {
    @ApiModelProperty(notes = "Denotes if paragraph is heading (bold)")
    private boolean heading;
    @ApiModelProperty(notes = "Paragraph text")
    private String paragraphText;
}

