package eu.delfin.controllers;

import eu.delfin.api.model.ArticleDto;
import eu.delfin.services.ArticleServiceImpl;
import io.swagger.annotations.Api;
import org.springframework.hateoas.Link;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

@Api(tags="Article Controller", description = "Handles article related requests")
@CrossOrigin
@RestController
@RequestMapping(ArticleController.BASE_PATH + "/{articleId}")
public class ArticleController {
    public static final String BASE_PATH = "/article";

    private ArticleServiceImpl articleService;

    public ArticleController(ArticleServiceImpl articleService) {
        this.articleService = articleService;
    }

    @GetMapping
    public ResponseEntity<ArticleDto> handleGetArticle(@PathVariable Long articleId) {
        Optional<ArticleDto> articleOpt = articleService.getArticleFromInternet(articleId.toString());
        if (articleOpt.isPresent()) {
            ArticleDto articleDtoResult = articleOpt.get();
            Link self = linkTo(ArticleController.class, articleId).withSelfRel();
            articleDtoResult.add(self);
            return ResponseEntity.ok(articleDtoResult);
        }
        return ResponseEntity.notFound().build();
    }
}