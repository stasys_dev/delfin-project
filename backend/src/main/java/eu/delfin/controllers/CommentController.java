package eu.delfin.controllers;

import eu.delfin.api.model.CommentDto;
import eu.delfin.api.model.CommentPageDto;
import eu.delfin.api.model.CommentPageListDto;
import eu.delfin.services.CommentService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

@Api(tags="Comments Controller", description = "Handles article related requests")
@CrossOrigin
@RestController
@RequestMapping(path = CommentController.BASE_PATH)
public class CommentController {
    public static final String BASE_PATH = ArticleController.BASE_PATH + "/{articleId}/comments";

    private CommentService commentService;

    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @GetMapping
    public CommentPageListDto getAllCommentPages(@PathVariable Long articleId) {
        List<CommentPageDto> commentPageDtos = commentService.getListOfAvailableCommentPages(articleId.toString())
                .stream()
                .peek(commentPageDto -> commentPageDto.add(
                        linkTo(CommentController.class, articleId).slash(commentPageDto.getPageIndex())
                                .withRel("commentPage")))
                .collect(Collectors.toList());
        return new CommentPageListDto(commentPageDtos);
    }

    @GetMapping(path = "/{pageIndex}")
    public List<CommentDto> getCommentsOnPage(@PathVariable Long articleId, @PathVariable Integer pageIndex) {
        return commentService.getCommentsOnPage(articleId.toString(), pageIndex);
    }
}
