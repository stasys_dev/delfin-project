package eu.delfin.mappers;

import eu.delfin.api.model.CommentDto;
import eu.delfin.parsers.data.Comment;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface CommentMapper {
    CommentMapper INSTANCE = Mappers.getMapper(CommentMapper.class);

    CommentDto mapToDto(Comment comment);
}
