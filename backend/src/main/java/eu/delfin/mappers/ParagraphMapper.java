package eu.delfin.mappers;

import eu.delfin.api.model.ParagraphDto;
import eu.delfin.parsers.data.Paragraph;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface ParagraphMapper {
    ParagraphMapper INSTANCE = Mappers.getMapper(ParagraphMapper.class);

    ParagraphDto mapToDto(Paragraph paragraph);
}
