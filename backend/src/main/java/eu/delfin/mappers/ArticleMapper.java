package eu.delfin.mappers;

import eu.delfin.api.model.ArticleDto;
import eu.delfin.parsers.data.Article;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(uses = {ParagraphMapper.class}, componentModel = "spring")
public interface ArticleMapper {
    ArticleMapper INSTANCE = Mappers.getMapper(ArticleMapper.class);

    ArticleDto mapToDto(Article article);
}
