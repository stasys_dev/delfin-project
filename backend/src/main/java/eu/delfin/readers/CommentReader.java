package eu.delfin.readers;

public interface CommentReader {
    String getCommentPageContentOrderByNewest(String articleId, int page);
}
