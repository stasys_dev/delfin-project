package eu.delfin.readers;

import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class CommentReaderImpl implements CommentReader {
    private static final String COMMENTS_ORDER_BY_NEWEST_URL = "https://www.delfi.lt/a/%s?com=1&reg=0&s=2&no=%s";

    @Override
    public String getCommentPageContentOrderByNewest(String articleId, int page) {
        int index = calculateIndex(page);
        String url = String.format(COMMENTS_ORDER_BY_NEWEST_URL, articleId, index);
        Optional<String> contentOpt = Utils.getHtmlContent(url);
        return contentOpt.orElse("");
    }

    private static int calculateIndex(int page) {
        return page <= 0 ? 0 : page * 20;
    }
}
