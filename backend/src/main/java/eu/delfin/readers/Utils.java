package eu.delfin.readers;

import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Optional;

class Utils {
    private static final Logger LOG = LoggerFactory.getLogger(Utils.class);

    private static final String MSG_FAILED_TO_RETRIEVE = "Failed to retrieve article. Page url %s";

    public static Optional<String> getHtmlContent(String url) {
        try {
            return Optional.of(Jsoup.connect(url).get().outerHtml());
        } catch (IOException e) {
            LOG.error(String.format(MSG_FAILED_TO_RETRIEVE, url), e);
            return Optional.empty();
        }
    }
}
