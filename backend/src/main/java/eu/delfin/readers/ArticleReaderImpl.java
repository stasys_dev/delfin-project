package eu.delfin.readers;

import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class ArticleReaderImpl implements ArticleReader {
    private static final String ARTICLE_URL = "https://www.delfi.lt/a/%s";
    
    public Optional<String> getArticlePageContent(String articleId) {
        String articleUrl = String.format(ARTICLE_URL, articleId);
        return Utils.getHtmlContent(articleUrl);
    }
}