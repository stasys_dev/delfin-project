package eu.delfin.readers;

import java.util.Optional;

public interface ArticleReader {
    Optional<String> getArticlePageContent(String articleId);
}
