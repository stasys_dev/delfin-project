package eu.delfin.error;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
    private Logger LOG = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    private static final String MSG_UNEXPECTED_EXCEPTION = "";

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler({Throwable.class})
    public void handleUnexpectedException(Throwable ex) {
        this.LOG.error(MSG_UNEXPECTED_EXCEPTION, ex);
    }
}
