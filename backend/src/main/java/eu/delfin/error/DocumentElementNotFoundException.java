package eu.delfin.error;

public class DocumentElementNotFoundException extends RuntimeException {
    private String failedCssQuery;

    public DocumentElementNotFoundException(String expectedQuery) {
        this.failedCssQuery = expectedQuery;
    }

    public String getFailedCssQuery() {
        return this.failedCssQuery;
    }
}
