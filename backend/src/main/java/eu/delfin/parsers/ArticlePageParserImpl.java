package eu.delfin.parsers;

import eu.delfin.error.DocumentElementNotFoundException;
import eu.delfin.parsers.data.Article;
import eu.delfin.parsers.data.Paragraph;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.*;

@Component
public class ArticlePageParserImpl implements ArticlePageParser {
    private static final Logger log = LoggerFactory.getLogger(ArticlePageParserImpl.class);
    private static final DateTimeFormatter DELFI_DATE_TIME_FORMATTER;
    private static final String QUERY_ARTICLE_PARAGRAPH = "div[itemprop=articleBody] p";
    private static final String QUERY_ARTICLE_PARAGRAPH_STRONG = "strong";
    private static final String QUERY_TITLE = "h1[itemprop=headline]";
    private static final String QUERY_ARTICLE_DESCRIPTION = "div[itemprop=description] > b";
    private static final String QUERY_AUTHOR_NAME = "div[itemprop=author]";
    private static final String QUERY_DELFI_AUTHOR_TITLE = "div.delfi-author-title";
    private static final String QUERY_DELFI_SOURCE_NAME = "div.delfi-source-name";
    private static final String QUERY_EXTERNAL_SOURCE_DATE = "div.source-date";
    private static final String QUERY_DELFI_SOURCE_DATE = "div.delfi-source-date";

    static {
        Map<Long, String> MONTH_MAP = new HashMap<>();
        MONTH_MAP.put(1L, "sausio");
        MONTH_MAP.put(2L, "vasario");
        MONTH_MAP.put(3L, "kovo");
        MONTH_MAP.put(4L, "balandžio");
        MONTH_MAP.put(5L, "gegužės");
        MONTH_MAP.put(6L, "birželio");
        MONTH_MAP.put(7L, "liepos");
        MONTH_MAP.put(8L, "rugpjūčio");
        MONTH_MAP.put(9L, "rugsėjo");
        MONTH_MAP.put(10L, "spalio");
        MONTH_MAP.put(11L, "lapkričio");
        MONTH_MAP.put(12L, "gruodžio");

        DELFI_DATE_TIME_FORMATTER = new DateTimeFormatterBuilder()
                .appendPattern("yyyy 'm'. ")
                .appendText(ChronoField.MONTH_OF_YEAR, MONTH_MAP)
                .appendPattern(" d 'd'. HH:mm")
                .toFormatter(Locale.US);
    }

    public Article parseArticle(String htmlSource) {
        Document doc = Jsoup.parse(htmlSource);
        Article article = new Article();
        article.setTitle(extractTitle(doc));
        article.setDescription(extractTextValue(QUERY_ARTICLE_DESCRIPTION, doc));
        article.setAuthorName(extractTextValue(QUERY_AUTHOR_NAME, doc));
        article.setAuthorTitle(extractTextValue(QUERY_DELFI_AUTHOR_TITLE, doc));
        article.setSource(extractTextValue(QUERY_DELFI_SOURCE_NAME, doc));
        article.setArticlePublishedDate(extractArticlePublishedDate(doc));
        article.getParagraphs().addAll(extractArticleContent(doc));
        return article;
    }

    private static List<Paragraph> extractArticleContent(Document doc) {
        Elements els = doc.select(QUERY_ARTICLE_PARAGRAPH);
        if (els == null) {
            throw new DocumentElementNotFoundException(QUERY_ARTICLE_PARAGRAPH);
        }
        List<Paragraph> result = new ArrayList<>(els.size());
        for (Element el : els) {
            String text = el.text().trim();
            if (text.isEmpty())
                continue;
            boolean header = isHeader(el);
            result.add(new Paragraph(header, text));
        }
        return result;
    }

    private static boolean isHeader(Element el) {
        log.debug(el.html());
        return el.selectFirst(QUERY_ARTICLE_PARAGRAPH_STRONG) != null;
    }

    private static String extractTitle(Document doc) {
        Element titleEl = doc.selectFirst(QUERY_TITLE);
        if (titleEl == null) {
            throw new DocumentElementNotFoundException(QUERY_TITLE);
        }
        return titleEl.text().trim();
    }

    private static String extractTextValue(String query, Document doc) {
        Element el = doc.selectFirst(query);
        return el != null ? el.text().trim() : null;
    }

    private static LocalDateTime extractArticlePublishedDate(Document doc) {
        Element dateEl = doc.selectFirst(QUERY_EXTERNAL_SOURCE_DATE);
        if (dateEl == null) {
            dateEl = doc.selectFirst(QUERY_DELFI_SOURCE_DATE);
        }
        if (dateEl == null) {
            throw new DocumentElementNotFoundException(QUERY_EXTERNAL_SOURCE_DATE + " or " + QUERY_DELFI_SOURCE_DATE);
        }
        return parseArticleDate(dateEl.text().trim());
    }

    private static LocalDateTime parseArticleDate(String date) {
        return LocalDateTime.parse(date, DELFI_DATE_TIME_FORMATTER);
    }
}