package eu.delfin.parsers.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Paragraph {
    private boolean heading;
    private String paragraphText;
}
