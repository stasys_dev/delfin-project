package eu.delfin.parsers.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Comment {
    private String author;
    private String delfiId;
    private LocalDateTime date;
    private String commentText;
    private int likes;
    private int dislikes;
    private List<Comment> replies;
}
