package eu.delfin.parsers.data;

import lombok.Data;

@Data
public class CommentPage {
    private int pageIndex;
}
