package eu.delfin.parsers.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Article {
    private String title;
    private String description;
    private Long articleExternalId;
    private String authorName;
    private String authorTitle;
    private LocalDateTime articlePublishedDate;
    private String source;
    private List<Paragraph> paragraphs = new ArrayList<>();
}
