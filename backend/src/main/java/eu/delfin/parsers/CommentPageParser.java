package eu.delfin.parsers;

import eu.delfin.parsers.data.Comment;

import java.util.List;

public interface CommentPageParser {
    int getNumberOfCommentPages(String htmlPageContent);
    List<Comment> getCommentsOnPage(String htmlPageContent);
}
