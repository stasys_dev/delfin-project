package eu.delfin.parsers;

import eu.delfin.parsers.data.Article;

public interface ArticlePageParser {
    Article parseArticle(String htmlSource);
}
