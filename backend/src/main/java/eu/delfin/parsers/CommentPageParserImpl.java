package eu.delfin.parsers;

import eu.delfin.error.DocumentElementNotFoundException;
import eu.delfin.parsers.data.Comment;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class CommentPageParserImpl implements CommentPageParser {
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
    private static final String QUERY_SELECT_ALL_COMMENTS = "#comments-list > .comment-post";
    private static final String TEXT_COMMENT_DELETED = "comment-deleted";
    private static final String QUERY_COMMENT_REPLIES_SECTION = "ul.comments-list-replies";
    private static final String QUERY_COMMENT_REPLIES = "div.comment-post";
    private static final String ATTRIBUTE_NAME_DATA_PARENT_COMMENT = "data-parent-comment";
    private static final String QUERY_COMMENT_TEXT = "div.comment-content-inner";
    private static final String QUERY_COMMENT_AUTHOR = "div.comment-author";
    private static final String QUERY_COMMENT_COUNTRY_CODE = "div.comment-date span";
    private static final String QUERY_DISLIKE_NUMBER = "div.comment-votes-down span.comment-votes-count";
    private static final String QUERY_LIKE_NUMBER = "div.comment-votes-up span.comment-votes-count";
    private static final String QUERY_COMMENT_DATE_IP = "div.comment-date";
    private static final String QUERY_COMMENT_LIST_ROOT = "#comments-list";
    private static final String ATTR_NUMBER_OF_PARENT_COMMENTS = "data-count";
    private static final String QUERY_DELFI_ID = "data-post-id";

    @Override
    public int getNumberOfCommentPages(String htmlPageContent) {
        int totalNumberOfParentComments = getNumberOfParentComments(htmlPageContent);
        int numOfWholePages = totalNumberOfParentComments / 20;
        int numOfPartialPages = totalNumberOfParentComments % 20 == 0 ? 0 : 1;
        return numOfWholePages + numOfPartialPages;
    }

    @Override
    public List<Comment> getCommentsOnPage(String htmlPageContent) {
        Document doc = Jsoup.parse(htmlPageContent);
        Map<String, Comment> commentMap = extractAllParentComments(doc);
        mapCommentReplies(doc, commentMap);
        List<Comment> result = new ArrayList<>(commentMap.values());
        result.sort((o1, o2) -> o2.getDate().compareTo(o1.getDate()));
        return result;
    }

    private void mapCommentReplies(Document doc, Map<String, Comment> parentCommentMap) {
        Elements cEls = doc.select(QUERY_COMMENT_REPLIES_SECTION);
        for (Element cEl : cEls) {
            String parentDelfiId = cEl.attr(ATTRIBUTE_NAME_DATA_PARENT_COMMENT);
            if (parentDelfiId != null && !parentDelfiId.trim().isEmpty() && !isCommentNotDeleted(cEl)) {
                Elements rEls = cEl.select(QUERY_COMMENT_REPLIES);
                for (Element r : rEls) {
                    Comment reply = extractComment(r);
                    Comment parentComment = parentCommentMap.get(parentDelfiId);
                    if (parentComment.getReplies() == null) {
                        parentComment.setReplies(new ArrayList<>());
                    }
                    parentComment.getReplies().add(reply);
                }
            }
        }
    }

    private Map<String, Comment> extractAllParentComments(Document doc) {
        Elements cEls = doc.select(QUERY_SELECT_ALL_COMMENTS);
        if (cEls == null) {
            throw new DocumentElementNotFoundException(QUERY_SELECT_ALL_COMMENTS);
        } else {
            Map<String, Comment> result = new HashMap<>();
            for (Element cEl : cEls) {
                if (!isCommentNotDeleted(cEl)) {
                    Comment comment = extractComment(cEl);
                    result.put(comment.getDelfiId(), comment);
                }
            }

            return result;
        }
    }

    private boolean isCommentNotDeleted(Element cEl) {
        return cEl.classNames().contains(TEXT_COMMENT_DELETED);
    }

    private Comment extractComment(Element cEl) {
        Comment comment = new Comment();
        comment.setDelfiId(extractDelfiId(cEl));
        comment.setAuthor(extractAuthorName(cEl));
        comment.setDate(extractDateTime(cEl));
        comment.setCommentText(extractCommentText(cEl));
        comment.setLikes(extractNumberOfLikes(cEl));
        comment.setDislikes(extractNumberOfDislikes(cEl));
        return comment;
    }

    private String extractDelfiId(Element cEl) {
        return cEl.attr(QUERY_DELFI_ID).trim();
    }

    private String extractAuthorName(Element cEl) {
        Element el = cEl.select(QUERY_COMMENT_AUTHOR).first();
        return el != null ? el.text().trim() : "";
    }

    private String extractCommentText(Element cEl) {
        Element el = cEl.select(QUERY_COMMENT_TEXT).first();
        return el == null ? "" : el.text().trim();
    }

    private int extractNumberOfLikes(Element cEl) {
        Element el = cEl.select(QUERY_LIKE_NUMBER).first();
        if (el != null) {
            String voteNumber = el.text();
            return Integer.parseInt(voteNumber);
        } else {
            return -1;
        }
    }

    private int extractNumberOfDislikes(Element cEl) {
        Element el = cEl.select(QUERY_DISLIKE_NUMBER).first();
        if (el != null) {
            String voteNumber = el.text();
            return Integer.parseInt(voteNumber);
        } else {
            return -1;
        }
    }

    private LocalDateTime extractDateTime(Element cEl) {
        Element el = cEl.select(QUERY_COMMENT_DATE_IP).first();
        if (el == null) {
            return null;
        } else {
            String dateTimeIp = el.text().trim();
            String dateTime = dateTimeIp.split("IP:")[0];
            return LocalDateTime.parse(dateTime.trim(), DATE_TIME_FORMATTER);
        }
    }

    private int getNumberOfParentComments(String htmlContent) {
        Document doc = Jsoup.parse(htmlContent);
        Element commentListEl = doc.selectFirst(QUERY_COMMENT_LIST_ROOT);
        if (commentListEl != null) {
            String numOfParentCommentsStr = commentListEl.attr(ATTR_NUMBER_OF_PARENT_COMMENTS);
            if (!numOfParentCommentsStr.isEmpty()) {
                return Integer.parseInt(numOfParentCommentsStr);
            } else {
                throw new DocumentElementNotFoundException(QUERY_COMMENT_LIST_ROOT + " -> attribute "
                        +  ATTR_NUMBER_OF_PARENT_COMMENTS + " value");
            }
        } else {
            throw new DocumentElementNotFoundException(QUERY_COMMENT_LIST_ROOT);
        }
    }
}
