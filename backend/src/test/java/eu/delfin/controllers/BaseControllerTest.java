package eu.delfin.controllers;

import java.time.format.DateTimeFormatter;

class BaseControllerTest {
    protected final DateTimeFormatter DATE_TIME_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd");
}
