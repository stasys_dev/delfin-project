package eu.delfin.controllers;

import eu.delfin.api.model.CommentDto;
import eu.delfin.api.model.CommentPageDto;
import eu.delfin.common.CommentTestDataEnum;
import eu.delfin.common.CommentTestUtils;
import eu.delfin.services.CommentServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = {CommentController.class})
public class CommentControllerTest extends BaseControllerTest {
    private static final Long EXTERNAL_ID = 77851933L;
    private static final int COMMENT_PAGE = 5;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CommentServiceImpl commentService;

    @Test
    public void shouldReturnEmptyCommentsList() throws Exception {
        when(commentService.getListOfAvailableCommentPages(anyString())).thenReturn(Collections.emptyList());

        mockMvc.perform(
                    get(CommentController.BASE_PATH, EXTERNAL_ID))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.commentPages.length()", equalTo(0)));
    }

    @Test
    public void shouldReturnCommentsList() throws Exception {
        CommentPageDto cp1 = new CommentPageDto(0);
        CommentPageDto cp2 = new CommentPageDto(1);
        CommentPageDto cp3 = new CommentPageDto(2);
        List<CommentPageDto> commentPages = Arrays.asList(cp1, cp2, cp3);
        when(commentService.getListOfAvailableCommentPages(anyString())).thenReturn(commentPages);

        mockMvc.perform(
                    get(CommentController.BASE_PATH, EXTERNAL_ID))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.commentPages.length()", equalTo(3)))
                .andExpect(jsonPath("$.commentPages[0].pageIndex", equalTo(0)))
                .andExpect(jsonPath("$.commentPages[0].links[0].href",
                        endsWith(CommentController.BASE_PATH.replace("{articleId}",
                                EXTERNAL_ID.toString()) + "/" + cp1.getPageIndex())))
                .andExpect(jsonPath("$.commentPages[1].pageIndex", equalTo(1)))
                .andExpect(jsonPath("$.commentPages[1].links[0].href",
                        endsWith(CommentController.BASE_PATH.replace("{articleId}",
                                EXTERNAL_ID.toString()) + "/" + cp2.getPageIndex())))
                .andExpect(jsonPath("$.commentPages[2].pageIndex", equalTo(2)))
                .andExpect(jsonPath("$.commentPages[2].links[0].href",
                        endsWith(CommentController.BASE_PATH.replace("{articleId}",
                                EXTERNAL_ID.toString()) + "/" + cp3.getPageIndex())));
    }

    @Test
    public void shouldReturnEmptyList_When_CommentIndexNotFound() throws Exception {
        when(commentService.getCommentsOnPage(anyString(), anyInt())).thenReturn(Collections.emptyList());

        mockMvc.perform(
                    get(CommentController.BASE_PATH + "/{pageIndex}", EXTERNAL_ID, COMMENT_PAGE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()", equalTo(0)));
    }

    @Test
    public void shouldReturnCommentsOnPage() throws Exception {
        CommentDto commentDto2 = CommentTestUtils.createCommentDto(CommentTestDataEnum.COMMENT2, null);
        CommentDto commentDto1 = CommentTestUtils.createCommentDto(CommentTestDataEnum.COMMENT1,
                Collections.singletonList(commentDto2));
        when(commentService.getCommentsOnPage(anyString( ), anyInt()))
                .thenReturn(Collections.singletonList(commentDto1));

        mockMvc.perform(
                    get(CommentController.BASE_PATH + "/{pageIndex}", EXTERNAL_ID, COMMENT_PAGE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()", equalTo(1)))
                .andExpect(jsonPath("$[0].author", equalTo(CommentTestDataEnum.COMMENT1.authorName)))
                .andExpect(jsonPath("$[0].delfiId", equalTo(CommentTestDataEnum.COMMENT1.delfiId.toString())))
                .andExpect(jsonPath("$[0].date", equalTo(DATE_TIME_FORMAT.format(
                        CommentTestDataEnum.COMMENT1.date))))
                .andExpect(jsonPath("$[0].commentText", equalTo(CommentTestDataEnum.COMMENT1.text)))
                .andExpect(jsonPath("$[0].likes", equalTo(CommentTestDataEnum.COMMENT1.numOfLikes)))
                .andExpect(jsonPath("$[0].dislikes", equalTo(CommentTestDataEnum.COMMENT1.numOfDislikes)))
                .andExpect(jsonPath("$[0].replies.length()", equalTo(1)))
                .andExpect(jsonPath("$[0].replies[0].author", equalTo(
                        CommentTestDataEnum.COMMENT2.authorName)))
                .andExpect(jsonPath("$[0].replies[0].delfiId", equalTo(
                        CommentTestDataEnum.COMMENT2.delfiId.toString())))
                .andExpect(jsonPath("$[0].replies[0].date", equalTo(DATE_TIME_FORMAT.format(
                        CommentTestDataEnum.COMMENT2.date))))
                .andExpect(jsonPath("$[0].replies[0].commentText", equalTo(
                        CommentTestDataEnum.COMMENT2.text)))
                .andExpect(jsonPath("$[0].replies[0].likes", equalTo(
                        CommentTestDataEnum.COMMENT2.numOfLikes)))
                .andExpect(jsonPath("$[0].replies[0].dislikes", equalTo(
                        CommentTestDataEnum.COMMENT2.numOfDislikes)));
    }
}