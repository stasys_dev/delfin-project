package eu.delfin.controllers;

import eu.delfin.api.model.ArticleDto;
import eu.delfin.api.model.ParagraphDto;
import eu.delfin.services.ArticleServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Optional;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = {ArticleController.class})
public class ArticleControllerTest extends BaseControllerTest {
    private static final String TITLE = "title";
    private static final String DESCRIPTION = "description";
    private static final Long EXTERNAL_ID = 77851933L;
    private static final String AUTHOR_NAME = "Tony Kukoc";
    private static final String AUTHOR_TITLE = "Reporter";
    private static final LocalDateTime PUBLISHED_DATE = LocalDateTime.now();
    private static final String SOURCE = "Some News Paper";
    private static final boolean PARAGRAPH_IS_HEADING = true;
    private static final String PARAGRAPH_TEXT = "Some article text...";
    private static final ParagraphDto PARAGRAPH = new ParagraphDto(PARAGRAPH_IS_HEADING, PARAGRAPH_TEXT);

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ArticleServiceImpl articleService;

    @Test
    public void shouldReturnArticle() throws Exception {
        ArticleDto articleDto = new ArticleDto();
        articleDto.setTitle(TITLE);
        articleDto.setDescription(DESCRIPTION);
        articleDto.setArticleExternalId(EXTERNAL_ID);
        articleDto.setAuthorName(AUTHOR_NAME);
        articleDto.setAuthorTitle(AUTHOR_TITLE);
        articleDto.setArticlePublishedDate(PUBLISHED_DATE);
        articleDto.setSource(SOURCE);
        articleDto.setParagraphs(Collections.singletonList(PARAGRAPH));
        when(articleService.getArticleFromInternet(anyString())).thenReturn(Optional.of(articleDto));

        mockMvc.perform(
                get(ArticleController.BASE_PATH + "/333333"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.title", equalTo(TITLE)))
                .andExpect(jsonPath("$.description", equalTo(DESCRIPTION)))
                .andExpect(jsonPath("$.articleExternalId").value(equalTo(EXTERNAL_ID), Long.class))
                .andExpect(jsonPath("$.authorName", equalTo(AUTHOR_NAME)))
                .andExpect(jsonPath("$.authorTitle", equalTo(AUTHOR_TITLE)))
                .andExpect(jsonPath("$.articlePublishedDate", equalTo(DATE_TIME_FORMAT
                        .format(PUBLISHED_DATE))))
                .andExpect(jsonPath("$.source", equalTo(SOURCE)))
                .andExpect(jsonPath("$.paragraphs.length()", equalTo(1)))
                .andExpect(jsonPath("$.paragraphs[0].paragraphText", equalTo(PARAGRAPH_TEXT)))
                .andExpect(jsonPath("$.paragraphs[0].heading", equalTo(PARAGRAPH_IS_HEADING)));
    }

    @Test
    public void shouldReturn403_When_MalformedRequest() throws Exception {
        when(articleService.getArticleFromInternet(anyString())).thenReturn(Optional.empty());

        mockMvc.perform(
                    get(ArticleController.BASE_PATH + "/33333dfsdf3"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldReturn404_When_ArticleDoesNotExist() throws Exception {
        when(articleService.getArticleFromInternet(anyString())).thenReturn(Optional.empty());

        mockMvc.perform(
                    get(ArticleController.BASE_PATH + "/333333"))
                .andExpect(status().isNotFound());
    }
}