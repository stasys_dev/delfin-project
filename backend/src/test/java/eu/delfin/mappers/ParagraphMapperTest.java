package eu.delfin.mappers;

import eu.delfin.api.model.ParagraphDto;
import eu.delfin.parsers.data.Paragraph;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class ParagraphMapperTest implements BaseMapperTest {
    private static final String PARAGRAPH_TEXT = "Paragraph text";
    private static final boolean PARAGRAPH_IS_HEADER = true;

    @Autowired
    private ParagraphMapper paragraphMapper;

    @Test
    public void shouldMapToDto() {
        Paragraph paragraph = new Paragraph(PARAGRAPH_IS_HEADER, PARAGRAPH_TEXT);

        ParagraphDto paragraphDto = paragraphMapper.mapToDto(paragraph);

        assertEquals(PARAGRAPH_IS_HEADER, paragraphDto.isHeading());
        assertEquals(PARAGRAPH_TEXT, paragraphDto.getParagraphText());
    }
}