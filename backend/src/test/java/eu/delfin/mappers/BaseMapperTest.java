package eu.delfin.mappers;

import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(classes = {MapperConfiguration.class})
interface BaseMapperTest {
}
