package eu.delfin.mappers;

import eu.delfin.mappers.ArticleMapper;
import eu.delfin.mappers.CommentMapper;
import eu.delfin.mappers.ParagraphMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class MapperConfiguration {
    @Bean
    public ArticleMapper articleMapper() {
        return ArticleMapper.INSTANCE;
    }

    @Bean
    public ParagraphMapper paragraphMapper() {
        return ParagraphMapper.INSTANCE;
    }

    @Bean
    public CommentMapper commentMapper() {
        return CommentMapper.INSTANCE;
    }
}
