package eu.delfin.mappers;

import eu.delfin.api.model.CommentDto;
import eu.delfin.common.CommentTestDataEnum;
import eu.delfin.common.CommentTestUtils;
import eu.delfin.parsers.data.Comment;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class CommentMapperTest implements BaseMapperTest {
    @Autowired
    private CommentMapper commentMapper;

    @Test
    public void shouldMapToDto() {
        CommentTestDataEnum comment1Data = CommentTestDataEnum.COMMENT1;
        Comment comment1 = new Comment(comment1Data.authorName, comment1Data.delfiId.toString(), comment1Data.date,
                comment1Data.text, comment1Data.numOfLikes, comment1Data.numOfDislikes, null);
        CommentTestDataEnum comment2Data = CommentTestDataEnum.COMMENT2;
        Comment comment2 = new Comment(comment2Data.authorName, comment2Data.delfiId.toString(), comment2Data.date,
                comment2Data.text, comment2Data.numOfLikes, comment2Data.numOfDislikes, null);
        comment1.setReplies(Collections.singletonList(comment2));

        CommentDto comment1Dto = commentMapper.mapToDto(comment1);

        CommentTestUtils.assertCommentEquals(comment1Data, comment1Dto);
        assertEquals(1, comment1Dto.getReplies().size());
        CommentTestUtils.assertCommentEquals(comment2Data, comment1Dto.getReplies().get(0));
    }
}