package eu.delfin.mappers;

import eu.delfin.api.model.ArticleDto;
import eu.delfin.api.model.ParagraphDto;
import eu.delfin.parsers.data.Article;
import eu.delfin.parsers.data.Paragraph;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.Collections;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class ArticleMapperTest implements BaseMapperTest {
    private static final String TITLE = "Some title";
    private static final Long EXTERNAL_ARTICLE_ID = 443342L;
    private static final LocalDateTime PUBLISHED_DATE = LocalDateTime.now();
    private static final String AUTHOR_NAME = "Author name";
    public static final String AUTHOR_TITLE = "Author title";
    public static final String DESCRIPTION = "Description";
    public static final String SOURCE = "Source";
    private static final String PARAGRAPH_TEXT = "Paragraph text";
    private static final boolean PARAGRAPH_IS_HEADER = true;

    @Autowired
    ArticleMapper articleMapper;

    @Test
    public void shouldMapToDto() {
        Article article = new Article();
        article.setTitle(TITLE);
        article.setArticleExternalId(EXTERNAL_ARTICLE_ID);
        article.setArticlePublishedDate(PUBLISHED_DATE);
        article.setAuthorName(AUTHOR_NAME);
        article.setAuthorTitle(AUTHOR_TITLE);
        article.setDescription(DESCRIPTION);
        article.setSource(SOURCE);
        article.setParagraphs(Collections.singletonList(new Paragraph(PARAGRAPH_IS_HEADER, PARAGRAPH_TEXT)));

        ArticleDto articleDtoDto = articleMapper.mapToDto(article);

        assertEquals(TITLE, articleDtoDto.getTitle());
        assertEquals(EXTERNAL_ARTICLE_ID, articleDtoDto.getArticleExternalId());
        assertEquals(PUBLISHED_DATE, articleDtoDto.getArticlePublishedDate());
        assertEquals(AUTHOR_NAME, articleDtoDto.getAuthorName());
        assertEquals(AUTHOR_TITLE, articleDtoDto.getAuthorTitle());
        assertEquals(DESCRIPTION, articleDtoDto.getDescription());
        assertEquals(SOURCE, articleDtoDto.getSource());
        assertEquals(1, articleDtoDto.getParagraphs().size());
        ParagraphDto paragraphDto = articleDtoDto.getParagraphs().get(0);
        assertEquals(PARAGRAPH_IS_HEADER, paragraphDto.isHeading());
        assertEquals(PARAGRAPH_TEXT, paragraphDto.getParagraphText());
    }
}