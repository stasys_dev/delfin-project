package eu.delfin.common;

import eu.delfin.api.model.CommentDto;
import eu.delfin.parsers.data.Comment;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class CommentTestUtils {
    public static void assertCommentEquals(CommentTestDataEnum expected, Comment actual) {
        assertCommentEquals(expected, actual.getAuthor(), actual.getDate(), actual.getCommentText(), actual.getLikes(),
                actual.getDislikes());
    }

    public static void assertCommentEquals(CommentTestDataEnum expected, CommentDto actualDto) {
        assertCommentEquals(expected, actualDto.getAuthor(), actualDto.getDate(), actualDto.getCommentText(),
                actualDto.getLikes(), actualDto.getDislikes());
    }

    private static void assertCommentEquals(CommentTestDataEnum expected, String author, LocalDateTime date,
                                            String commentText, int likes, int dislikes) {
        assertEquals(expected.authorName, author);
        assertEquals(expected.date, date);
        assertEquals(expected.text, commentText);
        assertEquals(expected.numOfLikes, likes);
        assertEquals(expected.numOfDislikes, dislikes);
    }

    public static Comment createComment(CommentTestDataEnum data, List<Comment> replies) {
        return new Comment(data.authorName, data.delfiId.toString(), data.date, data.text, data.numOfLikes,
                data.numOfDislikes, replies);
    }

    public static CommentDto createCommentDto(CommentTestDataEnum data, List<CommentDto> replies) {
        return new CommentDto(data.authorName, data.delfiId.toString(), data.date, data.text, data.numOfLikes,
                data.numOfDislikes, replies);
    }
}
