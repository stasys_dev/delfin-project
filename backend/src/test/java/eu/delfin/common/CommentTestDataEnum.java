package eu.delfin.common;

import java.time.LocalDateTime;

public enum CommentTestDataEnum {
    COMMENT1("Author1", LocalDateTime.of(2018, 4, 29, 14, 19), "Comment text 1", 1, 2, 1111L),
    COMMENT2("Author2", LocalDateTime.of(2019, 4, 29, 14, 19), "Comment text 2", 3, 4, 2222L);

    public final String authorName;
    public final LocalDateTime date;
    public final String text;
    public final int numOfLikes;
    public final int numOfDislikes;
    public final Long delfiId;

    CommentTestDataEnum(String authorName, LocalDateTime date, String text, int numOfLikes,
                        int numOfDislikes, Long delfiId) {
        this.authorName = authorName;
        this.date = date;
        this.text = text;
        this.numOfLikes = numOfLikes;
        this.numOfDislikes = numOfDislikes;
        this.delfiId = delfiId;
    }
}
