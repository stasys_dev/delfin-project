package eu.delfin.services;

import eu.delfin.api.model.CommentDto;
import eu.delfin.api.model.CommentPageDto;
import eu.delfin.common.CommentTestDataEnum;
import eu.delfin.common.CommentTestUtils;
import eu.delfin.mappers.CommentMapper;
import eu.delfin.parsers.CommentPageParser;
import eu.delfin.parsers.data.Comment;
import eu.delfin.readers.CommentReader;
import eu.delfin.services.CommentService;
import eu.delfin.services.CommentServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

public class CommentServiceImplTest {
    private static final String ARTICLE_EXTERNAL_ID = "33333";
    private static final int COMMENT_PAGE = 5;
    private static final String COMMENT_PAGE_CONTENT = "";

    @Mock
    private CommentPageParser commentPageParser;
    @Mock
    private CommentReader commentReader;

    private CommentService commentService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        commentService = new CommentServiceImpl(commentPageParser, commentReader, CommentMapper.INSTANCE);
    }

    @Test
    public void shouldGetSingleCommentPage_When_ThereAreNoComments() {
        when(commentReader.getCommentPageContentOrderByNewest(ARTICLE_EXTERNAL_ID, 0)).thenReturn("");
        when(commentPageParser.getNumberOfCommentPages(ARTICLE_EXTERNAL_ID)).thenReturn(0);

        List<CommentPageDto> comments = commentService.getListOfAvailableCommentPages(ARTICLE_EXTERNAL_ID);

        assertEquals(1, comments.size());
    }

    @Test
    public void shouldGetAllCommentPages() {
        when(commentReader.getCommentPageContentOrderByNewest(ARTICLE_EXTERNAL_ID, 0)).thenReturn("");
        when(commentPageParser.getNumberOfCommentPages(anyString())).thenReturn(5);

        List<CommentPageDto> comments = commentService.getListOfAvailableCommentPages(ARTICLE_EXTERNAL_ID);

        assertEquals(5, comments.size());
        assertEquals(0, comments.get(0).getPageIndex());
        assertEquals(1, comments.get(1).getPageIndex());
        assertEquals(2, comments.get(2).getPageIndex());
        assertEquals(3, comments.get(3).getPageIndex());
        assertEquals(4, comments.get(4).getPageIndex());
    }

    @Test
    public void shouldGetEmptyCommentsList_When_NoCommentsPresent() {
        List<CommentDto> comments = commentService.getCommentsOnPage(ARTICLE_EXTERNAL_ID, COMMENT_PAGE);

        assertTrue(comments.isEmpty());
    }

    @Test
    public void shouldGetCommentsOnPage() {
        Comment comment2 = CommentTestUtils.createComment(CommentTestDataEnum.COMMENT2, null);
        Comment commentDto1 = CommentTestUtils.createComment(CommentTestDataEnum.COMMENT1,
                Collections.singletonList(comment2));
        when(commentReader.getCommentPageContentOrderByNewest(ARTICLE_EXTERNAL_ID, COMMENT_PAGE))
                .thenReturn(COMMENT_PAGE_CONTENT);
        when(commentPageParser.getCommentsOnPage(anyString())).thenReturn(Collections.singletonList(commentDto1));

        List<CommentDto> comments = commentService.getCommentsOnPage(ARTICLE_EXTERNAL_ID, COMMENT_PAGE);

        assertEquals(1, comments.size());
        CommentDto resultComment = comments.get(0);
        CommentTestUtils.assertCommentEquals(CommentTestDataEnum.COMMENT1, resultComment);
        assertEquals(1, resultComment.getReplies().size());
        CommentTestUtils.assertCommentEquals(CommentTestDataEnum.COMMENT2, resultComment.getReplies().get(0));
    }
}