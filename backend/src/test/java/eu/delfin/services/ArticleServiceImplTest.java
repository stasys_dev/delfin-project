package eu.delfin.services;

import eu.delfin.api.model.ArticleDto;
import eu.delfin.mappers.ArticleMapper;
import eu.delfin.parsers.ArticlePageParser;
import eu.delfin.parsers.data.Article;
import eu.delfin.readers.ArticleReader;
import eu.delfin.services.ArticleService;
import eu.delfin.services.ArticleServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ArticleServiceImplTest {
    private static final Long ARTICLE_EXTERNAL_ID = 33333L;
    private static final String CONTENT = "";
    private static final Article ARTICLE = new Article();

    @Mock
    private ArticleReader articleReader;
    @Mock
    private ArticlePageParser articlePageParser;

    private ArticleService articleService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        articleService = new ArticleServiceImpl(articleReader, articlePageParser, ArticleMapper.INSTANCE);
    }

    @Test
    public void shouldReturnResult() {
        when(articleReader.getArticlePageContent(anyString())).thenReturn(Optional.of(CONTENT));
        when(articlePageParser.parseArticle(CONTENT)).thenReturn(ARTICLE);

        Optional<ArticleDto> result = articleService.getArticleFromInternet(ARTICLE_EXTERNAL_ID.toString());

        verify(articleReader).getArticlePageContent(ARTICLE_EXTERNAL_ID.toString());
        assertNotNull(result);
        assertTrue(result.isPresent());
        ArticleDto articleDto = result.get();
        assertEquals(ARTICLE_EXTERNAL_ID, articleDto.getArticleExternalId());
    }

    @Test
    public void shouldReturnEmpty_When_NoHtmlContent() {
        when(articleReader.getArticlePageContent(anyString())).thenReturn(Optional.empty());

        Optional result = articleService.getArticleFromInternet("23232");

        assertNotNull(result);
        assertFalse(result.isPresent());
    }
}