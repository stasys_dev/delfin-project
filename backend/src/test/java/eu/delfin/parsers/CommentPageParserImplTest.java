package eu.delfin.parsers;

import eu.delfin.parsers.data.Comment;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import static org.junit.Assert.assertEquals;

public class CommentPageParserImplTest {
    private static final int TOTAL_NUMBER_OF_COMMENTS = 20;
    private static final Map<String, Integer> COMMENT_REPLY_COUNT_MAP = new HashMap<>();
    static {
        COMMENT_REPLY_COUNT_MAP.put("137430820", 1);
        COMMENT_REPLY_COUNT_MAP.put("137430774", 2);
        COMMENT_REPLY_COUNT_MAP.put("137430740", 1);
        COMMENT_REPLY_COUNT_MAP.put("137430674", 1);
        COMMENT_REPLY_COUNT_MAP.put("137430568", 1);
        COMMENT_REPLY_COUNT_MAP.put("137430386", 1);
        COMMENT_REPLY_COUNT_MAP.put("137430308", 3);
    }

    private CommentPageParser commentPageParser;

    @Before
    public void setUp() {
        commentPageParser = new CommentPageParserImpl();
    }

    @Test
    public void shouldGetNumberOfCommentPages() {
        String content = new Scanner(CommentPageParserImplTest.class.getClassLoader()
                .getResourceAsStream("article2_unregistered_comments.html")).useDelimiter("\\Z").next();

        int numOfCommentPages = commentPageParser.getNumberOfCommentPages(content);

        assertEquals(12, numOfCommentPages);
    }

    @Test
    public void getCommentsOnPage() {
        String content = new Scanner(CommentPageParserImplTest.class.getClassLoader()
                .getResourceAsStream("article1_comments_page_all_comments.html")).useDelimiter("\\Z").next();

        List<Comment> commentsParsed = commentPageParser.getCommentsOnPage(content);
        assertEquals(TOTAL_NUMBER_OF_COMMENTS, commentsParsed.size());
    }

    private enum CommentTestDataEnum {
        COMMENT1("Vatiniai", LocalDateTime.of(2018, 4, 29, 14, 27),
                "Nu ka,Vadas tirpsta", 1, 0),
        COMMENT18("Tai va", LocalDateTime.of(2018, 4, 29, 14, 1),
                "O varge vargeli,net savaitgaliais nebeapsieiname be temos apie Rusiją.Pradedant visokių saldžiūnų " +
                        "propaganda ir gąsdinimais,baigiant straipsniais be autoriaus pavardės ir taip du,trys,o " +
                        "kartais ir daugiau straipsnių per dieną.Ir kam tai naudinga,nesuprantu nors pasiusk?Patys " +
                        "susmukę mėšle virš žvejybinių batų aulų,bet aktualiausia tema tai kaip gyvena kaimynai." +
                        "Korėjos pusiasalyje net septyniasdešimt metų buvę priešais spaudžia vienas kitam ranką,o mes" +
                        " kaip ožiai pastatę ragus.",
                1, 5),
        COMMENT18_3("a", LocalDateTime.of(2018, 4, 29, 14, 19),
                "Pasak biografo Dmitrijaus Volkogonovo, tarp Vladimiro Iljičiaus Uljanovo (Lenino) protėvių buvo " +
                        "rusai, kalmukai, švedai, vokiečiai, žydai.", 0, 0),
        COMMENT20("O KUR RASTI MINČIŲ KLINIKĄ?",
                LocalDateTime.of(2018, 4, 29, 14, 0), "VIEN BAISUMAI...", 1, 0);

        final String authorName;
        final LocalDateTime date;
        final String text;
        final int numOfLikes;
        final int numOfDislikes;

        CommentTestDataEnum(String authorName, LocalDateTime date, String text, int numOfLikes,
                            int numOfDislikes) {
            this.authorName = authorName;
            this.date = date;
            this.text = text;
            this.numOfLikes = numOfLikes;
            this.numOfDislikes = numOfDislikes;
        }

    }
}