package eu.delfin.parsers;

import eu.delfin.parsers.data.Article;
import eu.delfin.parsers.data.Paragraph;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

import static org.junit.Assert.*;

public class ArticlePageParserImplTest {
    private static final String ARTICLE_TITLE
            = "Netikėtas signalas Rusijai: į Europą permestas JAV galios simbolis čia gali užsibūti";
    private static final String ARTICLE_DESCRIPTION
            = "JAV karinės galios simboliais vadinami lėktuvnešiai paprastai strategiškai dislokuojami visame " +
            "pasaulyje taip, kad greitai galėtų pasiekti norimą vietą. Vienas tokių lėktuvnešių šią savaitę atplaukė " +
            "į Viduržemio jūrą. Čia jis neplanuotai gali likti ir ilgiau dėl kelių rimtų priežasčių.";
    private static final String ARTICLE_AUTHOR_NAME = "Vaidas Saldžiūnas";
    private static final String ARTICLE_AUTHOR_TITLE = "Korespondentas gynybos temomis";
    private static final LocalDateTime ARTICLE_DATE = LocalDateTime.of(2018, 4, 28, 19, 1);
    private static final int ARTICLE_NUMBER_OF_PARAGRAPHS = 35;
    private static final Set<Integer> ARTICLE_HEADER_INDEXES = new HashSet<>(Arrays.asList(2, 9, 16, 22, 29));

    private ArticlePageParser articlePageParser;

    @Before
    public void setUp() {
        articlePageParser = new ArticlePageParserImpl();
    }

    @Test
    public void shouldParseArticle() {
        String content = new Scanner(
                ArticlePageParserImplTest.class.getClassLoader().getResourceAsStream("article1.html"),
                "UTF-8")
                .useDelimiter("\\Z").next();

        Article article = articlePageParser.parseArticle(content);

        assertEquals(ARTICLE_TITLE, article.getTitle());
        assertEquals(ARTICLE_DESCRIPTION, article.getDescription());
        assertEquals(ARTICLE_AUTHOR_NAME, article.getAuthorName());
        assertEquals(ARTICLE_AUTHOR_TITLE, article.getAuthorTitle());
        assertEquals(ARTICLE_DATE, article.getArticlePublishedDate());
        assertEquals(ARTICLE_NUMBER_OF_PARAGRAPHS, article.getParagraphs().size());
        for (int i = 0; i < article.getParagraphs().size(); i++) {
            Paragraph p = article.getParagraphs().get(i);
            assertFalse(p.getParagraphText().isEmpty());
            if (ARTICLE_HEADER_INDEXES.contains(i)) {
                assertTrue(p.isHeading());
            } else {
                assertFalse(p.isHeading());
            }
        }
    }
}